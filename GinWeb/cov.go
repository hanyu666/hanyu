package main

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"os"
)

func GetData() (int64, int64) {
	// 打开json文件
	jsonFile, err := os.Open("tmp4.json")
	if err != nil {
		fmt.Println(err)
	}
	// 关闭文件
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	type Datadetail struct {
		Confirm int64
	}
	type Data struct {
		Name  string
		Today Datadetail
		Total Datadetail
	}

	var user Data
	var a, b int64
	json.Unmarshal([]byte(byteValue), &user)

	if user.Name == "香港" {
		a = int64(user.Total.Confirm)
		b = int64(user.Today.Confirm)

	}
	return a, b
}

func main() {
	//创建一个默认的路由引擎
	r := gin.Default()
	confirm1, confirm2 := GetData()
	fmt.Println(confirm1, confirm2)
	//GET:请求方法；/api/covid19/confirm请求的路径
	//当客户端以GET方法请求/api/covid19/confirm路径时，会执行后面的匿名函数
	r.GET("/api/covid19/confirm", func(c *gin.Context) {
		//c.JSON:返回json格式的数据   gin.H封装了生成json数据的工具
		c.JSON(http.StatusOK, gin.H{
			"today_confirm": confirm1,
			"total_confirm": confirm2,
		})
	})
	//启动HTTP服务，默认在0.0.0.0:8080启动服务
	r.Run() // listen and serve on 0.0.0.0:8080 (for windows “localhost:8080”)
}
