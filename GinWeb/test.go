package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	//创建一个默认的路由引擎
	r := gin.Default()

	//localhost:8080配置路由
	r.GET("/", func(c *gin.Context) {
		//http.StatusOK 请求成功的状态码（这里=200）
		c.JSON(http.StatusOK, gin.H{
			"message": "Hello,Gin!",
		})
	})

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	//启动一个web服务
	//listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
	//r.Run()默认:8080
	// Listening and serving HTTP on :8000
	r.Run(":8000")
}
