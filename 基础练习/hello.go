package main

import (
	"fmt"
	"unsafe"
)

var a1, b1 int
var c1 string

const (
	a2 = "abc"
	b2 = len(a2)
	c2 = unsafe.Sizeof(a2)
)

func main() { // '{'不能单独一行
	fmt.Println("Hello World!")
	fmt.Print("Google" + "Run")

	//全局变量赋值
	a1, b1, c1 = 5, 7, "abc"

	// %d是整型 %s是字符串
	var stockcode = 123
	var enddate = "2020-12-31"
	var url = "Code = %d&endDate = %s"
	var target_url = fmt.Sprintf(url, stockcode, enddate)
	fmt.Println(target_url)

	// 声明一个变量并初始化（不初始化变量默认为零值,bool的零值为false,字符串为空）
	// 使用$var获取变量var的内存地址
	var a = "RUNOOB"
	var b int
	var c bool
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)

	intVal := 1 //:=相当于声明变量，此句相当于 var intVal int; intVal = 1,这种不带声明格式的只能在函数体中出现
	fmt.Println(intVal)

	//常量的使用
	const LENGTH = 10
	const width = 5
	var area int
	const a1, b1, c1 = 1, false, "str" //多重赋值

	area = LENGTH * width
	fmt.Printf("面积为 ：%d", area)
	println()
	println(a1, b1, c1)

	//常量可以用len(),cap(),unsafe.Sizeof()函数计算表达式的值。常量表达式中，函数必须是内置函数。
	println(a2, b2, c2)

	// iota在const关键字出现时被重置为0，const中每新增一行常量声明将使iota计数一次
	const (
		a3 = iota //0
		b3        //1
		c3        //2
		d  = "ha" //独立值, iota +=1
		e         //"ha"	 iota += 1
		f  = 100  //iota += 1
		g         //100 iota += 1
		h  = iota //7, 恢复计数
		i         //8
	)
	fmt.Println(a3, b3, c3, d, e, f, g, h, i)

	// 条件控制语句
	//var a4 = 10
	var a4 = 100

	if a4 < 20 {
		fmt.Printf("a4 小于 20\n")
	} else {
		fmt.Printf("a 不小于 20\n")
	}
	fmt.Printf("a4 的值为：%d\n", a4)

	var grade = "B"
	var marks = 90

	switch marks {
	case 90:
		grade = "A"
	case 80:
		grade = "B"
	case 50, 60, 70:
		grade = "C"
	default:
		grade = "D"
	}
	fmt.Printf("你的等级是 %s\n", grade)

	//select每个case都必须是一个通信
	//所有channel表达式都会被求值
	//所有被发送的表达式都会被求值
	//如果任意某个通信可以进行，它就执行，其他被忽略
	//如果有多个case都可以运行，select会随机公平的选出一个执行，其他不会执行
	//否侧：
	//	如果有default子句，则执行该语句
	//	如果没有default子句，select将堵塞，直到某个通信可以运行
	var cc1, cc2, cc3 chan int
	var i1, i2 int
	select {
	case i1 = <-cc1:
		fmt.Printf("received ", i1, " from c1\n")
	case cc2 <- i2:
		fmt.Printf("sent ", i2, " to c2\n")
	case i3, ok := (<-cc3):
		if ok {
			fmt.Printf("received ", i3, " from c3\n")
		} else {
			fmt.Printf("c3 is closed\n")
		}
	default:
		fmt.Printf("no communication\n")

	}
}
