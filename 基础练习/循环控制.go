package main

import (
	"fmt"
	"math"
)

func Sqrt(x float64) float64 {
	i := 0
	z := 1.0
	for i < 10 {
		z -= (z*z - x) / (2 * z)
		i++
	}
	return z
}

//修改循环条件，使得当值停止改变（或改变非常小）的时候退出循环。
func Sqrt_new(x float64) (float64, int) {
	z := 1.0
	i := 0
	y := 0.0
	for i >= 0 {
		z -= (z*z - x) / (2 * z)
		if math.Abs(z-y) < 1e-5 {
			break
		}
		y = z
		i++
	}
	return z, i
}

func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(math.Sqrt(2))
	fmt.Println(Sqrt_new(2))
}
