package main

import "fmt"

// 返回一个“返回int的函数”
//斐波那契数列：F(0)=0,F(1)=1,F(n)=F(n-1)+F(n-2)
func fibonacci() func() int {
	a := 0
	b := 1
	return func() int {
		c := a
		a = b
		b = b + c
		return c
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}
