package main

import (
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

func rot13(x byte) byte {
	if x >= 'A' && x <= 'M' || x >= 'a' && x <= 'm' {
		x += 13
	} else {
		x -= 13
	}
	return x
}

func (f rot13Reader) Read(b []byte) (int, error) {
	n, err := f.r.Read(b)
	for i := 0; i < n; i++ {
		b[i] = rot13(b[i])
	}
	return n, err
}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}
